﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FizzBuzz.WebApp.Models
{
    public class IndexModel
    {
        [Required(ErrorMessage = "Please enter any positive integer number between 1 and 1000")]
        [Range(1, 1000, ErrorMessage = "Input should be positive integer between 1 and 1000")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter any positive integer number between 1 and 1000")]

        public int? InputData { get; set; }

        public IList<string> Messages { get; set; }
    }
}
