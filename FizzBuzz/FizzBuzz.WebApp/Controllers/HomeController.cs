﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzz.Service;
using FizzBuzz.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzz.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITextService _messages;
        public HomeController(ITextService messages)
        {
            _messages = messages;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(IndexModel indexModel)
        {
            if (ModelState.IsValid)
            {
                int inputData = (int)indexModel.InputData;
                IList<string> lstMessages = _messages.GetMessages(inputData);
                IndexModel result = new IndexModel
                {
                    Messages = lstMessages,
                    InputData = indexModel.InputData
                };
                return View(result);
            }
            else
            {
                return View();
            }
           
        }


    }

}