using FizzBuzz.Service;
using FizzBuzz.Service.Rule;
using FizzBuzz.Service.TextServices;
using FizzBuzz.WebApp.Controllers;
using FizzBuzz.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Xunit;

namespace FizzBuzz.WebApp.Test
{
    public class HomeControllerTest
    {
        private readonly ITextService _messages;

        public HomeControllerTest(ITextService messages)
        {
            _messages = messages;
        }
        [Fact]
        public void Test_Index_ReturnView()
        {
            //Arrange
            var result = new HomeController(_messages);

            //Act
            var actualResult = result.Index();

            //Assert
            Assert.IsAssignableFrom<IActionResult>(actualResult);
        }

        [Theory, MemberData(nameof(InputAndExpectedData))]
        public void Test_Index_PostMethod_ReturnsView(IndexModel model)
        {
            //Arrange
            var result = new HomeController(_messages);

            //Act
            var actualResult = result.Index(model);

            //Assert
            Assert.IsAssignableFrom<IActionResult>(actualResult);
        }

        public static IEnumerable<object[]> InputAndExpectedData =>
           new List<object[]>
           {
                new object[] { new IndexModel { InputData = 10 } }
           };
    }
}
