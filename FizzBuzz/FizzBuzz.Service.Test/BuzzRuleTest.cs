﻿using FizzBuzz.Service.DayOfCurrentWeek;
using FizzBuzz.Service.Rule;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.Service.Test
{
    public class BuzzRuleTest
    {
        private readonly IDayOfTheWeek dayOfTheWeek;
        public BuzzRuleTest()
        {
            dayOfTheWeek = new DayOfTheWeek();
        }
        [Fact]
        public void TestBuzzRule_IsNumberMatched_WhenNumberDivisibleBy5()
        {
            //Arrange
            var result = new BuzzRule(dayOfTheWeek);

            //Act
            bool actualResult = result.IsNumberMatched(20);

            //Assert
            Assert.True(actualResult);
        }

        [Fact]
        public void TestBuzzRule_IsNumberMatched_WhenNumberNotDivisibleBy5()
        {
            //Arrange
            var result = new BuzzRule(dayOfTheWeek);

            //Act
            bool actualResult = result.IsNumberMatched(7);

            //Assert
            Assert.False(actualResult);
        }

        [Fact]
        public void TestBuzzRule_GetReplacedWord()
        {
            //Arrange
            var expectedValue = "buzz";
            var result = new BuzzRule(dayOfTheWeek);

            //Act
            var actualResult = result.GetReplacedWord();

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }
    }
}
