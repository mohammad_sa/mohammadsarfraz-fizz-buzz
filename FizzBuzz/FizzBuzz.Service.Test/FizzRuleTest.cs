﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FizzBuzz.Service.Rule;
using FizzBuzz.Service.DayOfCurrentWeek;

namespace FizzBuzz.Service.Test
{
    public class FizzRuleTest
    {
        private readonly IDayOfTheWeek dayOfTheWeek;
        public FizzRuleTest()
        {
            dayOfTheWeek = new DayOfTheWeek();
        }
        [Fact]
        public void TestFizzRule_IsNumberMatched_WhenNumberDivisibleBy3()
        {
            //Arrange

            var result = new FizzRule(dayOfTheWeek);

            //Act
            bool actualResult = result.IsNumberMatched(6);

            //Assert
            Assert.True(actualResult);
        }

        [Fact]
        public void TestFizzRule_IsNumberMatched_WhenNumberNotDivisibleBy3()
        {
            //Arrange
            var result = new FizzRule(dayOfTheWeek);

            //Act
            bool actualResult = result.IsNumberMatched(5);

            //Assert
            Assert.False(actualResult);
        }

        [Fact]
        public void TestFizzRule_GetReplacedWord()
        {
            //Arrange
            var expectedValue = "fizz";
            var result = new FizzRule(dayOfTheWeek);

            //Act
            var actualResult = result.GetReplacedWord();

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }
    }
}
