using FizzBuzz.Service.Rule;
using FizzBuzz.Service.TextServices;
using System;
using System.Collections.Generic;
using Xunit;

namespace FizzBuzz.Service.Test
{
    public class TextServiceTest
    {
        private readonly IEnumerable<IRule> rules;
        public TextServiceTest()
        {
            rules = new List<IRule>
            {
                new FizzRule(),
                new BuzzRule(),
                new FizzBuzzRule()
            };
        }
        [Theory, MemberData(nameof(InputAndExpectedData))]
        public void Test_GenerateFizzBuzzMethod(int inputValue1, List<string> expectedValue)
        {
            //Arrange            
            var result = new TextService(rules);

            //Act
            IList<string> actualResult = result.GetMessages(inputValue1);

            //Assert
            Equals(expectedValue, actualResult);
        }

        public static IEnumerable<object[]> InputAndExpectedData =>
           new List<object[]>
           {
                new object[] { 5, new List<string> { "1", "2", "fizz", "4", "buzz" } }
           };

        [Theory]
        [InlineData(30, "fizz buzz")]
        [InlineData(33, "fizz")]
        [InlineData(35, "buzz")]
        [InlineData(29, "29")]
        public void Test_ProcessFizzBuzzDataMethod(int inputValue1, string expectedValue)
        {
            //Arrange
            var result = new TextService(rules);

            //Act
            var actualResult = result.ProcessFizzBuzzData(inputValue1);

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }
    }
}
