﻿using FizzBuzz.Service.DayOfCurrentWeek;
using FizzBuzz.Service.Rule;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.Service.Test
{
    public class FizzBuzzRuleTest
    {
        private readonly IDayOfTheWeek dayOfTheWeek;
        public FizzBuzzRuleTest()
        {
            dayOfTheWeek = new DayOfTheWeek();
        }
        [Fact]
        public void TestFizzBuzzRule_IsNumberMatched_WhenNumberDivisibleBy3And5()
        {
            //Arrange
            var result = new FizzBuzzRule(dayOfTheWeek);

            //Act
            bool actualResult = result.IsNumberMatched(30);

            //Assert
            Assert.True(actualResult);
        }

        [Fact]
        public void TestFizzBuzzRule_IsNumberMatched_WhenNumberNotDivisibleBy3And5()
        {
            //Arrange
            var result = new FizzBuzzRule(dayOfTheWeek);

            //Act
            bool actualResult = result.IsNumberMatched(10);

            //Assert
            Assert.False(actualResult);
        }
        [Fact]
        public void TestFizzBuzzRule_GetReplacedWord()
        {
            //Arrange
            var expectedValue = "fizz buzz";
            var result = new FizzBuzzRule(dayOfTheWeek);

            //Act
            var actualResult = result.GetReplacedWord();

            //Assert
            Assert.Equal(expectedValue, actualResult);
        }

    }
}
