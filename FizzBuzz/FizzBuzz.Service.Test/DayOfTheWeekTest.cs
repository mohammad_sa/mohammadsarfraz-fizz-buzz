﻿using FizzBuzz.Service.DayOfCurrentWeek;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace FizzBuzz.Service.Test
{
    public class DayOfTheWeekTest
    {
        [Fact]
        public void GetCurrentDayTest()
        {
            //Arrange
            DayOfWeek expectedDay = DateTime.Today.DayOfWeek;
            var result = new DayOfTheWeek();

            //Act
            DayOfWeek actualDay = result.GetCurrentDay();

            //Assert
            Assert.Equal(expectedDay, actualDay);
        }
    }
}
