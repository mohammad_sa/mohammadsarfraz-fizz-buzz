﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Service
{
    public interface IDayOfTheWeek
    {
        DayOfWeek GetCurrentDay();
    }
}
