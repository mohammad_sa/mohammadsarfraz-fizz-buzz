﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Service
{
    public interface ITextService
    {
         IList<string> GetMessages(int inputNumber);

    }
}
