﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Service.Rule
{
    public class FizzBuzzRule : IRule
    {
        private readonly IDayOfTheWeek _dayOfTheWeek;
        public FizzBuzzRule(IDayOfTheWeek dayOfTheWeek)
        {
            _dayOfTheWeek = dayOfTheWeek;
        }
        public bool IsNumberMatched(int inputNumber)
        {
            return inputNumber % 3 == 0 && inputNumber % 5 == 0;
        }

        public string GetReplacedWord()
        {
            return (_dayOfTheWeek.GetCurrentDay() == DayOfWeek.Wednesday) ? "wizz wuzz" : "fizz buzz";
        }
    }
}
