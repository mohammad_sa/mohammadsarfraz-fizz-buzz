﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Service.Rule
{
    public class BuzzRule : IRule
    {
        private readonly IDayOfTheWeek _dayOfTheWeek;
        public BuzzRule(IDayOfTheWeek dayOfTheWeek)
        {
            _dayOfTheWeek = dayOfTheWeek;
        }
        public bool IsNumberMatched(int inputNumber)
        {
            return inputNumber % 3 != 0 && inputNumber % 5 == 0;
        }

        public string GetReplacedWord()
        {
            return (_dayOfTheWeek.GetCurrentDay() == DayOfWeek.Wednesday) ? "wuzz" : "buzz";
        }
    }
}
