﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FizzBuzz.Service
{
    public interface IRule
    {
        bool IsNumberMatched(int inputNumber);
        string GetReplacedWord();
    }
}
